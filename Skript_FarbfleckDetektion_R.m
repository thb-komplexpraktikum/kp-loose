%% Komplexpraktikum SS 2019
% ========================================================================
% Autor: H. Loose, 03/2011
% Version:         03/2011, 03/2016
% ========================================================================
%
%
%% Rücksetzen der Umgebung
clc, clear, close all
%% 
pfad = 'C:\Lehre\KP\INF\Loesung\BeispielLoesung\';
% pfad = 'd:\Lehre\KomplexpraktikumINF\BeispielLoesung\';
pfad = '/home/sebaran/Downloads/KPINF_FarbErkennung/BeispielLoesung/';
% % name = [pfad  'B5.JPG'];
% % name = [pfad  'Blume.JPG'];
name = [pfad  'Flag_of_Germany.png'];
% name = [pfad  'SafeColors.png'];
% % name = [pfad  'Bsp027.JPG'];
[rgb,hsv] = FarbModelleDarstellung(name,1);
%% Setzen von Parametern
% Für ROT
nhood = ones(5); % Nachbarschaft für morphologische Operationen
nrpixel = 200; % Mindestgröße von interessierenden Regionen
farbe ='rot';
kanaele = [1,2,3];
%% Auswertung aus dem RGB-Farbmodell
%% Erkennung von farbe-Bereichen
% farbWert = [0.7,0.15,0.15]; % RGB: rot
% farbDistanz = 0.31;
% farbWichtung = [1 0.95 0.9]; %Wichtung der Farbkanäle
farbWert = [0.9,0.9,0.0]; % RGB: gelb
farbDistanz = 0.2;
farbWichtung = [1 0.95 0.9]; %Wichtung der Farbkanäle
bw_rgb = FarbErkennungDistanz_RGB(rgb,name,farbe,farbWert,farbDistanz,farbWichtung);
%% Erkennung und Schließen der Regionen
% nach Öffnen, Aureißer-Bereinigung (zu kleine Gebiete) und Schließen
bw_rgb1 = BereichErkennen(bw_rgb,name,farbe,nhood,nrpixel);
%% Bestimmen von Regionen und deren Eigenschaften 
% regions_rgb = bwconncomp(bw_rgb1);
disp('Ergebnisse für RGB-Farberkennung:');
regions_rgb = bwlabel(bw_rgb1);
props_rgb = regionprops(regions_rgb);
disp(['Anzahl der Regionen: ' num2str(size(props_rgb,1))]);
for k = 1 : size(props_rgb,1),
    disp([num2str(k) ':  Größe der Region [px]: ' num2str(props_rgb(k).Area) ...
        '  Zentralpunkte der Region [px]: ', num2str(props_rgb(k).Centroid)]);
end
%% Auswertung aus dem HSV-Farbmodell
%% Erkennung von farbe-Bereichen
% Kanaele H, S, V liegen zwischen sw_1 und sw_2
% farbWert = [1,0.75,0.8]; % RGB: rot
% farbDistanz = 0.3;
% farbWichtung = [0.25 1 1]; %Wichtung der Farbkanäle
farbWert = [0.1666,1,0.9]; % RGB: gelb
farbDistanz = 0.2;
farbWichtung = [0.25 1 1]; %Wichtung der Farbkanäle
[bw_hsv] = FarbErkennungDistanz_HSV(hsv,name,farbe,farbWert,farbDistanz,farbWichtung);
%% Erkennung und Schließen der Regionen
bw_hsv1 = BereichErkennen(bw_hsv,name,farbe,nhood,nrpixel);
%% Bestimmen von Regionen und deren Eigenschaften 
% regions_hsv = bwconncomp(bw_hsv1);
disp('Ergebnisse für HSV-Farberkennung:');
regions_hsv = bwlabel(bw_hsv1);
props_hsv = regionprops(regions_hsv);
disp(['Anzahl der Regionen: ' num2str(size(props_hsv,1))]);
for k = 1 : size(props_hsv,1),
    disp([num2str(k) ':  Größe der Region [px]: ' num2str(props_hsv(k).Area) ...
        '  Zentralpunkte der Region [px]: ', num2str(props_hsv(k).Centroid)]);
end
%% Ergebnisdarstellung
figure, subplot(1,5,1), imshow(rgb); title('Original')
subplot(1,5,2), imshow(maskImage( rgb, bw_rgb1 )); title('Auswahl aus RGB')
subplot(1,5,3), imshow(maskImage( rgb, bw_hsv1 ));  title('Auswahl aus HSV')
subplot(1,5,4), imshow(maskImage( rgb, ~bw_rgb1 )); title('Rest aus RGB')
subplot(1,5,5), imshow(maskImage( rgb, ~bw_hsv1 )); title('Rest aus HSV')
