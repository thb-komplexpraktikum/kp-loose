function [bild,bild_hsv] = FarbModelleDarstellung(name,rs)
%% Komplexpraktikum SS 2010/11
% ========================================================================
% Autor: H. Loose, 03/2011
% Version:         03/2011
% ========================================================================
%
%%
bild = imresize(im2double(imread(name)),rs);
% imshow(bild);
bild_sw = rgb2gray(bild);
%% Alle Anzeigen
figure('Name',name); subplot(3,4,1)
imshow(bild); title('Original');
subplot(3,4,5); imshow(bild_sw); title('SW');
clear bild_sw;
%% RGB-Kanalanzeige
subplot(3,4,2); imshow(bild(:,:,1));title('Rot');
subplot(3,4,6); imshow(bild(:,:,2)); title('Gr�n');
subplot(3,4,10); imshow(bild(:,:,3)); title('Blau'); 
%% HSV-Berechnen und Anzeigen
bild_hsv = rgb2hsv(bild);
subplot(3,4,3); imshow(bild_hsv(:,:,1)); title('Hue');
subplot(3,4,7); imshow(bild_hsv(:,:,2)); title('Saturation');
subplot(3,4,11); imshow(bild_hsv(:,:,3)); title('Brightness');
%% Farbverteilung
farbleiste = 0 : 0.001 : 1;
farbbild = ones(1001,1001,3);
for i = 1:1001, farbbild(:,i,1) = farbleiste(i); end
%% Darstellung der Grauwertverteilung
subplot(3,4,9); imshow(farbbild(:,:,1)); title('SW: 0 .. 1');
axis on
%% Darstellung der Farbverteilung im H-Kanal
subplot(3,4,12); imshow(hsv2rgb(farbbild)); title('Hue: 0 .. 1');
axis on
%% H-Kanal als Farbdarstellung mit S=V=1
bild_hsv1 = bild_hsv; bild_hsv1(:,:,2:3) = 1;
subplot(3,4,4); imshow(hsv2rgb(bild_hsv1));title('(Hue,1,1)');
end