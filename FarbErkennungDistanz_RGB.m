function [bw_rgb] = FarbErkennungDistanz_RGB(rgb,name,farbe,...
    farbWert,farbDistanz,farbWichtung)
%% Komplexpraktikum SS 2016
% ========================================================================
% Autor: H. Loose, 03/2011
% Version:         03/2011, 03/2016
% ========================================================================
% Erzeugen eines Binärbildes, in dem die interessanten Bereiche auf 1
% gesetzt sind
model = 'RGB';
%figure('Name',[name model farbe]);
%subplot(2,4,1); imshow(rgb); title(['Original']);
%% farbe aus R
distanz = farbDistanz*farbWichtung;
tmp = zeros(size(rgb,1),size(rgb,2));
bin_rgb_ = zeros(size(rgb,1),size(rgb,2),3);
for k = 1 : 3,
    tmp( abs(rgb(:,:,k)-farbWert(k))<distanz(k)) = 1;
    bin_rgb_(:,:,k) =  tmp; 
    tmp(:,:) = 0;
    %subplot(2,4,1+k), imshow(bin_rgb_(:,:,k)), title(['in Kanal ' model(k)])
end
bin_rgb = bin_rgb_(:,:,1).* bin_rgb_(:,:,2).*bin_rgb_(:,:,3);
%subplot(2,4,5); imshow(bin_rgb); title([farbe ' aus RGB']);
%% Umwandeln in Binärdatei
bw_rgb = im2bw(bin_rgb,0.5);
end