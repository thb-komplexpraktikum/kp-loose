function [bw_hsv] = FarbErkennungDistanz_HSV(hsv,name,farbe,...
    farbWert,farbDistanz,farbWichtung)
%% Komplexpraktikum SS 2016
% ========================================================================
% Autor: H. Loose, 03/2011
% Version:         03/2011, 03/2016
% ========================================================================
% Erzeugen eines Binärbildes, in dem die interessanten Bereiche auf 1
% gesetzt sind
model = 'HSV';
%figure('Name',[name model farbe]);
%subplot(2,4,1); imshow(hsv); title(['Original']);
distanz = farbDistanz*farbWichtung;
tmp = zeros(size(hsv,1),size(hsv,2));
bin_ = zeros(size(hsv,1),size(hsv,2),3);
for k = 1 : 3,
    if k == 1,
       if  farbWert(k) + distanz(k) > 1
            tmp( (hsv(:,:,k) < farbWert(k)+distanz(k)-1) | ...
                (hsv(:,:,k) > farbWert(k)-distanz(k) ) ) = 1;
       else if farbWert(k) - distanz(k) < 0,
                tmp( (hsv(:,:,k) > farbWert(k)-distanz(k)+1) | ...
                    (hsv(:,:,k) < distanz(k)+farbWert(k) ) ) = 1;
            else
                tmp( abs(hsv(:,:,k)-farbWert(k))<distanz(k)) = 1;
            end
       end
    else
        tmp( abs(hsv(:,:,k)-farbWert(k))<distanz(k)) = 1;
    end
    bin_(:,:,k) =  tmp; 
    tmp(:,:) = 0;
    %subplot(2,4,1+k), imshow(bin_(:,:,k)), title(['in Kanal ' model(k)])
end
bin_hsv = bin_(:,:,1).* bin_(:,:,2).*bin_(:,:,3);
%subplot(2,4,5); imshow(bin_hsv); title([farbe ' aus HSV']);
%% Umwandeln in Binärdatei
bw_hsv = im2bw(bin_hsv,0.5);
end