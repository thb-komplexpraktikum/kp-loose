function [ bild_mask ] = maskImage( bild, mask )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
for k = 1 : 3,
    tmp = bild(:,:,k);
    tmp(mask == false) = 0;
    bild(:,:,k) = tmp;
end
bild_mask = bild;
end

