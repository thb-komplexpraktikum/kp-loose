%% Rücksetzen der Umgebung
clc, clear, close all
%%
pfad = pwd;
name = [pfad  '/Flag_of_Jordan.png'];
[rgb,hsv] = FarbModelleDarstellung(name,1);
%% Setzen von Parametern
nhood = ones(5); % Nachbarschaft für morphologische Operationen
nrpixel = 200; % Mindestgröße von interessierenden Regionen
kanaele = [1,2,3];

[r, g, b] = (findAllColors(name, nrpixel));
colors = [r g b];
rgb_regions = [];
hsv_regions = [];

rows = size(colors, 1);

%%
for color = 1 : rows
    colorname = ['Farbe ' num2str(color)];
    farbDistanz = 0.2;
    farbWichtung = [1 0.95 0.9];
    [bw_rgb1, rgb_region] = rgb_analysis(rgb, name, colorname, colors, color, nhood, nrpixel, farbDistanz, farbWichtung);
    rgb_regions = [rgb_regions,rgb_region];
    
    farbDistanz = 0.2;
    farbWichtung = [0.25 1 1];
    [bw_hsv1, hsv_region] = hsv_analysis(hsv, name, colors, color, colorname, nhood, nrpixel, farbDistanz, farbWichtung);
    hsv_regions = [hsv_regions, hsv_region];
    result(rgb, bw_rgb1, bw_hsv1);
end

[x,y,z] = size(rgb);
pic_size = x*y;
conclusion(colors, rgb_regions, 'rgb', pic_size);
conclusion(colors, hsv_regions, 'hsv', pic_size);


function [bw_rgb1, rgb_region] = rgb_analysis(rgb, name, colorname, colors, color, nhood, nrpixel, farbDistanz, farbWichtung)
    %% Auswertung aus dem RGB-Farbmodell
    %% Erkennung von farbe-Bereichen
    farbWert = colors(color,:);
    bw_rgb = FarbErkennungDistanz_RGB(rgb,name, colorname, farbWert, farbDistanz, farbWichtung);
    %% Erkennung und Schließen der Regionen
    % nach öffnen, Aureißer-Bereinigung (zu kleine Gebiete) und Schließen
    bw_rgb1 = BereichErkennen(bw_rgb,name,colorname,nhood,nrpixel);
    %% Bestimmen von Regionen und deren Eigenschaften

    regions_rgb = bwlabel(bw_rgb1);
    props_rgb = regionprops(regions_rgb);

    %disp('Ergebnisse für RGB-Farberkennung:');
    %display_color(props_rgb);


    rgb_region.color = colorname;
    rgb_region.props = props_rgb;
end

function [bw_hsv1, hsv_region] = hsv_analysis(hsv, name, colors, color, colorname, nhood, nrpixel, farbDistanz, farbWichtung)
    %% Auswertung aus dem HSV-Farbmodell
    %% Erkennung von farbe-Bereichen
    farbWert =  rgb2hsv(colors(color,:));
    [bw_hsv] = FarbErkennungDistanz_HSV(hsv,name, colorname, farbWert, farbDistanz, farbWichtung);
    %% Erkennung und Schließen der Regionen
    bw_hsv1 = BereichErkennen(bw_hsv,name,colorname,nhood,nrpixel);
    %% Bestimmen von Regionen und deren Eigenschaften
    % regions_hsv = bwconncomp(bw_hsv1);

    regions_hsv = bwlabel(bw_hsv1);
    props_hsv = regionprops(regions_hsv);

    %disp('Ergebnisse für HSV-Farberkennung:');
    %display_color(props_hsv);

    hsv_region.color = colorname;
    hsv_region.props = props_hsv;
end

function [] = display_color(props)
  disp(['Anzahl der Regionen: ' num2str(size(props,1))]);
  for k = 1 : size(props,1)
      disp([num2str(k) ':  Größe der Region [px]: ' num2str(props(k).Area) ...
          '  Zentralpunkte der Region [px]: ', num2str(props(k).Centroid)]);
  end
end

function [] = result(rgb, bw_rgb1, bw_hsv1)
    %% Ergebnisdarstellung
    figure, subplot(1,5,1), imshow(rgb); title('Original')
    subplot(1,5,2), imshow(maskImage( rgb, bw_rgb1 )); title('Auswahl aus RGB')
    subplot(1,5,3), imshow(maskImage( rgb, bw_hsv1 ));  title('Auswahl aus HSV')
    subplot(1,5,4), imshow(maskImage( rgb, ~bw_rgb1 )); title('Rest aus RGB')
    subplot(1,5,5), imshow(maskImage( rgb, ~bw_hsv1 )); title('Rest aus HSV')
end

function [] = conclusion(colors, regions, colorspace, pic_size)
    total_regions = 0;
    max_regions = zeros(size(colors,1),1);
    min_regions = zeros(size(colors,1),1);


    rows = size(regions,2);
    for color = 1 : rows
        region = regions(color);
        count_regions = size(region.props,1);
        disp([colorspace ': ' region.color ': Anzahl der Regionen: ' num2str(count_regions)]);
        total_regions = total_regions + count_regions;

        if(size(region.props) > 0)
            max_area = max([region.props.Area]);
            min_area = min([region.props.Area]);
            percentage = sum([region.props.Area]) / pic_size * 100;
        else
            max_area = 0;
            min_area = 0;
            percentage = 0;
        end
        max_regions(color) = max_area;
        min_regions(color) = min_area;
        disp([colorspace ': ' region.color ': Größte Region: ' num2str(max_area)]);
        disp([colorspace ': ' region.color ': Kleinste Region: ' num2str(min_area)]);
        disp([colorspace ': ' region.color ': Anteil am Bild: ' num2str(percentage)]);
        disp(' ');
    end

    disp([colorspace ': ' 'Gesamt: Anzahl Regionen: ' num2str(total_regions)]);
    disp([colorspace ': ' 'Gesamt: Größte Region: ' num2str(max(max_regions))]);
    disp([colorspace ': ' 'Gesamt: Kleineste Region: ' num2str(min(min_regions))]);
    disp(' ');
end

function [r,g,b] = findAllColors(name, threshold)
    rgbImage = imread(name);
    [rows, columns, ~] = size(rgbImage);

    redChannel = rgbImage(:, :, 1);
    greenChannel = rgbImage(:, :, 2);
    blueChannel = rgbImage(:, :, 3);

    histogram = zeros(256,256,256);
    for column = 1: columns
        for row = 1 : rows
            rIndex = redChannel(row, column) + 1;
            gIndex = greenChannel(row, column) + 1;
            bIndex = blueChannel(row, column) + 1;
            histogram(rIndex, gIndex, bIndex) = histogram(rIndex, gIndex, bIndex) + 1;
        end
    end

    histogram = histogram > threshold;

    [r, g, b] = ind2sub(size(histogram),find(histogram));
    r = (r - 1) / 255;
    g = (g -1) / 255;
    b = (b -1) / 255;
end
