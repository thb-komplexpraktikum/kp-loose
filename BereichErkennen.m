function [bw_end] = BereichErkennen(bw,name,farbe,nhood,nrpixel)
%% Komplexpraktikum SS 2010/11
% ========================================================================
% Autor: H. Loose, 03/2011
% Version:         03/2011
% ========================================================================
%
% figure('Name',[name ' ' farbe]);
%subplot(2,4,5); imshow(bw); title(['Maske']);
%% Fl�chen werden ge�ffnet
bw1 = imopen(bw,nhood);
%subplot(2,4,6); imshow(bw1); title(['nach imopen']);
%% das dem Bin�rbild werden Fl�cehn mit weniger al nrpixel entfernt
bw2=bwareaopen(bw1,nrpixel);
%subplot(2,4,7); imshow(bw2); title(['nach bwareaopen']);
%% Fl�chen werden geschlossen
bw_end=imclose(bw2,nhood);
%subplot(2,4,8); imshow(bw_end); title(['nach imclose']);
end
